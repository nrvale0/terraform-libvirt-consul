variable "libvirt_uri" {}

variable "network_address_range" {}

variable "cloud_image_url" {}

variable "server_memory" {
  default = "1024"
}

variable "server_vcpu" {
  default = "1"
}

variable "server_count" {
  default = "3"
}

variable "consul_version" {
  default = "1.3.0"
}

variable "custom_user-data" {
  default = ""
}

variable "ssh_pub_key" {}

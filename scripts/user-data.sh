[ ! -n "$DEBUG" ] || set -x

: ${CONSUL_VERSION="${consul_version}"}
: ${CONSUL_DOWNLOAD_URI="https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip"}

set -eu

function onerr {
    echo 'Cleaning up after error...'
    exit -1
}
trap onerr ERR


function deps() {
    echo "Checking for required dependencies..."
    set +e
    command -v wget && command -v unzip
    if [ "$?" != "0" ]; then
	export DEBIAN_FRONTEND=noninteractive
	export DEBCONF_NONINTERACTIVE_SEEN=true
	apt-get update
	apt-get install -y wget unzip
    fi
    set -e
}


function consul_download() {
    echo "Downloading Consul..."
    wget -q -c -O /usr/local/bin/consul.zip "${CONSUL_DOWNLOAD_URI}"
    unzip -o -d /usr/local/bin /usr/local/bin/consul.zip
}


function consul_configure() {
    mkdir -p /etc/consul.d /var/consul

    cat <<EOF | tee /etc/consul.d/00-default.hcl
ui = true
data_dir = "/var/consul"
EOF
    consul validate /etc/consul.d

    cat <<EOF | tee /etc/systemd/system/consul.service

EOF
    systemctl daemon-reload
    systemctl enable consul.service
    systemctl strat consul.service
}


function main() {
    deps
    consul_download
    consul_configure
 }


main

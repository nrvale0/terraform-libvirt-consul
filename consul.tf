resource "libvirt_volume" "consul-image" {
  name   = "consul-image.img"
  format = "qcow2"
  source = "${var.cloud_image_url}"
}

resource "libvirt_volume" "consul-server-root-volume" {
  count          = "${var.server_count}"
  name           = "consul-server-root-${count.index}"
  base_volume_id = "${libvirt_volume.consul-image.id}"
}

data "template_file" "consul_server_bootstrap_sh" {
  template = "${file("${path.module}/templates/user-data.sh.tpl")}"

  vars {
    consul_version = "${var.consul_version}"
    logic          = "${file("${path.module}/scripts/user-data.sh")}"
  }
}

data "template_cloudinit_config" "consul_server_cloudinit" {
  gzip          = false
  base64_encode = false

  part {
    filename     = "cloud_init.cfg"
    content_type = "text/cloud-config"
    content = <<EOF
#cloud-config
ssh_authorized_keys:
  - "${var.ssh_pub_key}"
EOF
  }

  part {
    filename = "bootstrap.sh"
    content_type = "text/x-shellscript"
    content = "${data.template_file.consul_server_bootstrap_sh.rendered}"
  }
}

resource "libvirt_cloudinit_disk" "consul-server-user-data" {
  name      = "consul-server-cloud-init.iso"
  user_data = "${data.template_cloudinit_config.consul_server_cloudinit.rendered}"
}

resource "libvirt_domain" "consul-server" {
  count     = "${var.server_count}"
  name      = "consul-server-${count.index}"
  memory    = "${var.server_memory}"
  vcpu      = "${var.server_vcpu}"
  autostart = true

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_port = "1"
    target_type = "virtio"
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }

  network_interface {
    #    network_id = "${libvirt_network.consul-network.id}"
    hostname = "consul-server-${count.index}"
    bridge   = "br0"

    #wait_for_lease = true
  }

  disk {
    volume_id = "${element(libvirt_volume.consul-server-root-volume.*.id, count.index)}"
  }

  cloudinit = "${libvirt_cloudinit_disk.consul-server-user-data.id}"
}
